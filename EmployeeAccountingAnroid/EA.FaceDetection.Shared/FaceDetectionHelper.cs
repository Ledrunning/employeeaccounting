﻿using Microsoft.ProjectOxford.Face;
using ServiceHelpers;
using System;
using System.Linq;
using System.Threading.Tasks;

//TODO
//Create class to send on web api images and data
//Create field to enter API key and link if needed;

/// <summary>
/// This Universal Class maded for IOS, Android and UWP projects;
/// </summary>
namespace FaceDetection.Shared
{
    public static class FaceDetectionHelper
    {
        public static bool IsFaceRegistered { get; set; }

        public static bool IsInitialized { get; set; }

        public static string WorkspaceKey { get; set; }

        public static Action<string> GreetingsCallback { get; set; }

        /// <summary>
        /// Init method with Face API key
        /// </summary>
        /// <param name="throttled"></param>
        public static void Init(Action throttled = null)
        {
            FaceServiceHelper.ApiKey = "cdbfcca90e904f3fa12b5f277bba17b7";

            if (throttled != null)
                FaceServiceHelper.Throttled += throttled;

            WorkspaceKey = Guid.NewGuid().ToString();
            ImageAnalyzer.PeopleGroupsUserDataFilter = WorkspaceKey;
            FaceListManager.FaceListsUserDataFilter = WorkspaceKey;

            IsInitialized = true;
        }

        /// <summary>
        /// Method for face registering
        /// </summary>
        /// <returns></returns>
        public static async Task RegisterFaces()
        {
            try
            {
                var persongroupId = Guid.NewGuid().ToString();
                await FaceServiceHelper.CreatePersonGroupAsync(persongroupId,
                                                        "Xamarin",
                                                     WorkspaceKey);
                await FaceServiceHelper.CreatePersonAsync(persongroupId, "Осман Мазинов");

                var personsInGroup = await FaceServiceHelper.GetPersonsAsync(persongroupId);

                //await FaceServiceHelper.AddPersonFaceAsync(persongroupId, personsInGroup[0].PersonId,
                //                                       "https://upload.wikimedia.org/wikipedia/commons/d/d3/Albert_Einstein_Head.jpg", null, null);
                await FaceServiceHelper.AddPersonFaceAsync(persongroupId, personsInGroup[0].PersonId, "https://habrastorage.org/webt/of/ov/-l/ofov-lntlm_k6cvrsigqgv31avs.jpeg", null, null);

                await FaceServiceHelper.TrainPersonGroupAsync(persongroupId);

                IsFaceRegistered = true;
            }
            catch (FaceAPIException ex)

            {
                Console.WriteLine(ex.Message);
                IsFaceRegistered = false;
            }
        }

        /// <summary>
        /// Method for image capturing
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static async Task ProcessCameraCapture(ImageAnalyzer e)
        {
            var start = DateTime.Now;

            await e.DetectFacesAsync();

            if (e.DetectedFaces.Any())
            {
                await e.IdentifyFacesAsync();
                string greetingsText = GetGreettingFromFaces(e);

                if (e.IdentifiedPersons.Any())
                {
                    if (GreetingsCallback != null)
                    {
                        DisplayMessage(greetingsText);
                    }

                    Console.WriteLine(greetingsText);
                }
                else
                {
                    DisplayMessage("Личность не установлена. Зарегестрируйтесь.");
                    Console.WriteLine("Личность не установлена.");
                }
            }
            else
            {
                // DisplayMessage("No face detected.");

                Console.WriteLine("Лицо не обнаружено ");
            }

            TimeSpan latency = DateTime.Now - start;
            //var latencyString = string.Format("Face API : {0}ms", (int)latency.TotalMilliseconds);
            var latencyString = $"Задержка Face API : {(int)latency.TotalMilliseconds} ms";
            Console.WriteLine(latencyString);
        }

        /// <summary>
        /// Method to create greatings message on screen
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private static string GetGreettingFromFaces(ImageAnalyzer img)
        {
            if (img.IdentifiedPersons.Any())
            {
                string names = img.IdentifiedPersons.Count() > 1 ? string.Join(", ", img.IdentifiedPersons.Select(p => p.Person.Name)) : img.IdentifiedPersons.First().Person.Name;

                if (img.DetectedFaces.Count() > img.IdentifiedPersons.Count())
                {
                    // return string.Format("С возвращением, {0} и компания!", names);
                    return $"С возвращением, {names} и компания!";
                }
                else
                {
                    //return string.Format("Welcome back, {0}!", names);
                    return $"Приветсвуем, {names}!";
                }
            }
            else
            {
                if (img.DetectedFaces.Count() > 1)
                {
                    return "Здравствуйте, если бы я знал кого-то из вас по имени, я бы сказал...";
                }
                else
                {
                    return "Здравствуйте, если бы я знал вас по имени, я бы сказал кто вы ...";
                }
            }
        }

        /// <summary>
        /// Method to show greatings message on screen
        /// </summary>
        /// <param name="greetingsText"></param>
        private static void DisplayMessage(string greetingsText)
        {
            GreetingsCallback?.Invoke(greetingsText);
        }
    }
}