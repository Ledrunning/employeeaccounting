﻿/*
 * Ported to C# from https://github.com/googlesamples/android-vision/tree/master/visionSamples/FaceTracker
 * Ported by Nish Anil (Nish@microsoft.com)
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using Android.Content;
using Android.Gms.Vision;
using Android.Graphics;
using Android.Util;
using Android.Views;
using System;

namespace EA.Android.CustomControls
{
    /// <summary>
    /// Class for camera using
    /// </summary>
    public sealed class CameraSourcePreview : ViewGroup, ISurfaceHolderCallback
    {
        private static readonly string TAG = "CameraSourcePreview";

        private readonly Context _context;
        private readonly SurfaceView _surfaceView;
        private bool _startRequested;
        private bool _surfaceAvailable;
        private CameraSource _cameraSource;
        private GraphicOverlay _overlay;

        /// <summary>
        /// .ctor with activity context
        /// </summary>
        /// <param name="context"></param>
        /// <param name="attrs"></param>
        public CameraSourcePreview(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            _context = context;
            _startRequested = false;
            _surfaceAvailable = false;

            _surfaceView = new SurfaceView(context);
            _surfaceView.Holder.AddCallback(this);

            AddView(_surfaceView);
        }

        public void StartCameraSource(CameraSource cameraSource)
        {
            if (cameraSource == null)
            {
                StopCameraSource();
            }

            _cameraSource = cameraSource;

            if (_cameraSource != null)
            {
                _startRequested = true;
                StartIfReady();
            }
        }

        public void StartCameraSource(CameraSource cameraSource, GraphicOverlay overlay)
        {
            _overlay = overlay;
            StartCameraSource(cameraSource);
        }

        public void StopCameraSource()
        {
            if (_cameraSource != null)
            {
                _cameraSource.Stop();
            }
        }

        public void Release()
        {
            if (_cameraSource != null)
            {
                _cameraSource.Release();
                _cameraSource = null;
            }
        }

        private void StartIfReady()
        {
            if (_startRequested && _surfaceAvailable)
            {
                _cameraSource.Start(_surfaceView.Holder);
                if (_overlay != null)
                {
                    var size = _cameraSource.PreviewSize;
                    var min = Math.Min(size.Width, size.Height);
                    var max = Math.Max(size.Width, size.Height);
                    if (IsPortraitMode())
                    {
                        // Swap width and height sizes when in portrait, since it will be rotated by
                        // 90 degrees
                        _overlay.SetCameraInfo(min, max, _cameraSource.CameraFacing);
                    }
                    else
                    {
                        _overlay.SetCameraInfo(max, min, _cameraSource.CameraFacing);
                    }
                    _overlay.ClearOverlay();
                }
                _startRequested = false;
            }
        }

        private bool IsPortraitMode()
        {
            var orientation = _context.Resources.Configuration.Orientation;
            if (orientation == global::Android.Content.Res.Orientation.Landscape)
            {
                return false;
            }
            if (orientation == global::Android.Content.Res.Orientation.Portrait)
            {
                return true;
            }

            Log.Debug(TAG, "isPortraitMode returning false by default");
            return false;
        }

        public void SurfaceChanged(ISurfaceHolder holder, Format format, int width, int height)
        {
           
        }

        public void SurfaceCreated(ISurfaceHolder holder)
        {
            _surfaceAvailable = true;

            try
            {
                StartIfReady();
            }
            catch (Exception e)
            {
                Log.Error(TAG, "Could not start camera source.", e);
            }
        }

        public void SurfaceDestroyed(ISurfaceHolder holder)
        {
            _surfaceAvailable = false;
        }

        protected override void OnLayout(bool changed, int left, int top, int right, int bottom)
        {
            int width = 320;
            int height = 240;
            if (_cameraSource != null)
            {
                var size = _cameraSource.PreviewSize;
                if (size != null)
                {
                    width = size.Width;
                    height = size.Height;
                }
            }

            // Swap width and height sizes when in portrait, since it will be rotated 90 degrees
            if (IsPortraitMode())
            {
                int tmp = width;
                width = height;
                height = tmp;
            }

            int layoutWidth = right - left;
            int layoutHeight = bottom - top;

            // Computes height and width for potentially doing fit width.
            int childWidth = layoutWidth;
            int childHeight = (int)(((float)layoutWidth / (float)width) * height);

            // If height is too tall using fit width, does fit height instead.
            if (childHeight > layoutHeight)
            {
                childHeight = layoutHeight;
                childWidth = (int)(((float)layoutHeight / (float)height) * width);
            }

            for (int i = 0; i < ChildCount; ++i)
            {
                GetChildAt(i).Layout(0, 0, childWidth, childHeight);
            }

            try
            {
                StartIfReady();
            }
            catch (Exception e)
            {
                Log.Error(TAG, "Could not start camera source.", e);
            }
        }
    }
}