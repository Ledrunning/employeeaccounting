﻿using System;
using Android;
using Android.App;
using Android.Content.PM;
using Android.Gms.Common;
using Android.Gms.Vision;
using Android.Gms.Vision.Faces;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Support.V7.App;
using Android.Util;
using Android.Widget;
using EA.Android.CustomControls;
using FaceDetection.Shared;
using Exception = Java.Lang.Exception;

//TODO
// AddToOverlay button to change camera (Front/Back)
// Check Wi Fi or mobile internet connection
/// <summary>
/// Change string
/// TextAnalyticsHelper - link to API host: private const string ServiceBaseUri = "https://westus.api.cognitive.microsoft.com/";
/// to address that Microsoft gave you
/// Also You should do it in FaceServiceClient
/// </summary>
namespace EA.Android
{
    [Activity(Label = "CatchMe", MainLauncher = true, Icon = "@drawable/camera128x128", Theme = "@style/Theme.AppCompat.NoActionBar", ScreenOrientation = ScreenOrientation.FullSensor)]
    public class MainActivity : AppCompatActivity, MultiProcessor.IFactory
    {
        private static readonly string TAG = "FaceTracker";

        private CameraSource _cameraSource = null;

        private CameraSourcePreview _preview;
        private GraphicOverlay _graphicOverlay;

        public static string GreetingsText { get; set; }

        private static readonly int RC_HANDLE_GMS = 9001;
        //permission request codes need to be< 256
        private static readonly int RC_HANDLE_CAMERA_PERM = 2;
        TextView _txtCameraSource;
        private bool _cameraSourceFlag = true;

        /// <summary>
        /// Main function onCreate
        /// Initialize all data when app is run
        /// </summary>
        /// <param name="bundle"></param>
        protected async override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            _preview = FindViewById<CameraSourcePreview>(Resource.Id.preview);
            _graphicOverlay = FindViewById<GraphicOverlay>(Resource.Id.faceOverlay);

            ((ImageButton)FindViewById(Resource.Id.btnCameraFrontBack)).Click += btnChangeCamera_HandleClick;
            _txtCameraSource = (TextView) FindViewById(Resource.Id.txtCameraFrontBack);
            _txtCameraSource.Text = "Front camera";

            //greetingsText = FindViewById<TextView>(Resource.Id.greetingsTextView);

            if (ActivityCompat.CheckSelfPermission(this, Manifest.Permission.Camera) == Permission.Granted)
            {
                CreateCameraSource();
                try
                {
                    //FaceDetectionHelper.Init();
                    //FaceDetectionHelper.GreetingsCallback = (s) => { RunOnUiThread(() => GreetingsText = s); };
                    //await FaceDetectionHelper.RegisterFaces();
                }
                catch (Exception err)
                {
                    Log.Debug(TAG, err.Message);
                }
              
            }
            else
            {
                RequestCameraPermission();
            }


        }

        /// <summary>
        /// Change camera source handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChangeCamera_HandleClick(object sender, EventArgs e)
        {
            if(_cameraSourceFlag)
            {
                _txtCameraSource.Text = "Front camera";
                Toast.MakeText(this, "Front camera enabled!", ToastLength.Short).Show();
                CreateCameraSource();
                _cameraSourceFlag = false;
            }
            else
            {
                _txtCameraSource.Text = "Back camera";
                Toast.MakeText(this, "Back camera enabled!", ToastLength.Short).Show();
                CreateCameraSource();
                _cameraSourceFlag = true;
            }
        }

        protected override void OnResume()
        {
            base.OnResume();
            StartCameraSource();
            CreateCameraSource();
        }

        protected override void OnPause()
        {
            base.OnPause();
            _preview.StopCameraSource();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (_cameraSource != null)
            {
                _cameraSource.Release();
            }
        }

        /**
         * Request camera permissions 
         * 
         */
        private void RequestCameraPermission()
        {
            Log.Warn(TAG, "Camera permission is not granted. Requesting permission");

            var permissions = new string[] { Manifest.Permission.Camera };

            if (!ActivityCompat.ShouldShowRequestPermissionRationale(this,
                    Manifest.Permission.Camera))
            {
                ActivityCompat.RequestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
                return;
            }

            //Snackbar.Make(mGraphicOverlay, FaceDetection.Droid.Resource.String.permission_camera_rationale,
            //        Snackbar.LengthIndefinite)
            //        .SetAction(FaceDetection.Droid.Resource.String.ok, (o) => { ActivityCompat.RequestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM); })
            //        .Show();
        }


        /**
         * Creates and starts the camera.  Note that this uses a higher resolution in comparison
         * to other detection examples to enable the barcode detector to detect small barcodes
         * at long distances.
         */
        private void CreateCameraSource()
        {

            var context = Application.Context;
            FaceDetector detector = new FaceDetector.Builder(context)
                    .SetClassificationType(ClassificationType.All)
                    .Build();

            detector.SetProcessor(
                    new MultiProcessor.Builder(this)
                            .Build());

            if (!detector.IsOperational)
            {
                // Note: The first time that an app using face API is installed on a device, GMS will
                // download a native library to the device in order to do detection.  Usually this
                // completes before the app is run for the first time.  But if that download has not yet
                // completed, then the above call will not detect any faces.
                //
                // isOperational() can be used to check if the required native library is currently
                // available.  The detector will automatically become operational once the library
                // download completes on device.
                Log.Warn(TAG, "Face detector dependencies are not yet available.");
            }

            if (_cameraSourceFlag)
            {
                _cameraSource = new CameraSource.Builder(context, detector)
                    .SetRequestedPreviewSize(640, 480)
                                            .SetFacing(CameraFacing.Front)
                    .SetRequestedFps(30.0f)
                    .Build();
            }
            else
            {
                _cameraSource = new CameraSource.Builder(context, detector)
                    .SetRequestedPreviewSize(640, 480)
                                            .SetFacing(CameraFacing.Back)
                    .SetRequestedFps(30.0f)
                    .Build();
            }


        }

        

        /**
         * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
         * (e.g., because onResume was called before the camera source was created), this will be called
         * again when the camera source is created.
         */
        private void StartCameraSource()
        {

            // check that the device has play services available.
            int code = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(
                    this.ApplicationContext);
            if (code != ConnectionResult.Success)
            {
                Dialog dlg =
                        GoogleApiAvailability.Instance.GetErrorDialog(this, code, RC_HANDLE_GMS);
                dlg.Show();
            }

            if (_cameraSource != null)
            {
                try
                {
                    _preview.StartCameraSource(_cameraSource, _graphicOverlay);
                }
                catch (System.Exception e)
                {
                    Log.Error(TAG, "Unable to start camera source.", e);
                    _cameraSource.Release();
                    _cameraSource = null;
                }
            }
        }

        /// <summary>
        /// Create GraphicFaceTracker object
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Tracker Create(Java.Lang.Object item)
        {
            return new GraphicFaceTracker(_graphicOverlay, _cameraSource);
        }


        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            if (requestCode != RC_HANDLE_CAMERA_PERM)
            {
                Log.Debug(TAG, "Got unexpected permission result: " + requestCode);
                base.OnRequestPermissionsResult(requestCode, permissions, grantResults);

                return;
            }

            if (grantResults.Length != 0 && grantResults[0] == Permission.Granted)
            {
                Log.Debug(TAG, "Camera permission granted - initialize the camera source");
                // we have permission, so create the camerasource
                CreateCameraSource();
                return;
            }

            Log.Error(TAG, "Permission not granted: results len = " + grantResults.Length +
                    " Result code = " + (grantResults.Length > 0 ? grantResults[0].ToString() : "(empty)"));
        }
    }

}


