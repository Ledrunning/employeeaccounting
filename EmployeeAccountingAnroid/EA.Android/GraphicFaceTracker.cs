﻿using Android.Gms.Vision;
using Android.Gms.Vision.Faces;
using EA.Android.CustomControls;
using FaceDetection.Shared;
using ServiceHelpers;
using System;
using System.Threading.Tasks;

namespace EA.Android
{
    internal class GraphicFaceTracker : Tracker, CameraSource.IPictureCallback
    {
        private GraphicOverlay _overlay;
        private FaceGraphic _faceGraphic;
        private CameraSource _cameraSource = null;
        private bool _isProcessing = false;

        /// <summary>
        /// Constructor to create graphics and added camera source
        /// </summary>
        /// <param name="overlay"></param>
        /// <param name="cameraSource"></param>
        public GraphicFaceTracker(GraphicOverlay overlay, CameraSource cameraSource = null)
        {
            _overlay = overlay;
            _faceGraphic = new FaceGraphic(overlay);
            _cameraSource = cameraSource;
        }

        /// <summary>
        /// Overriden method from Google Tracker API
        /// </summary>
        /// <param name="id"></param>
        /// <param name="item"></param>
        public override void OnNewItem(int id, Java.Lang.Object item)
        {
            _faceGraphic.SetId(id);
            if (_cameraSource != null && !_isProcessing)
                _cameraSource.TakePicture(null, this);
        }

        /// <summary>
        /// Updatet screen
        /// </summary>
        /// <param name="detections"></param>
        /// <param name="item"></param>
        public override void OnUpdate(Detector.Detections detections, Java.Lang.Object item)
        {
            var face = item as Face;
            _overlay.AddToOverlay(_faceGraphic);
            _faceGraphic.UpdateFace(face);
        }

        /// <summary>
        /// RemoveFromOverlay rectangle from face when face is not found
        /// </summary>
        /// <param name="detections"></param>
        public override void OnMissing(Detector.Detections detections)
        {
            _overlay.RemoveFromOverlay(_faceGraphic);
        }

        public override void OnDone()
        {
            _overlay.RemoveFromOverlay(_faceGraphic);
        }

        /// <summary>
        /// Method to take a face in stream video
        /// </summary>
        /// <param name="data"></param>
        public void OnPictureTaken(byte[] data)
        {
            Task.Run(async () =>
            {
                try
                {
                    _isProcessing = true;

                    Console.WriteLine("face detected: ");

                    var imageAnalyzer = new ImageAnalyzer(data);
                    await FaceDetectionHelper.ProcessCameraCapture(imageAnalyzer);
                }
                finally
                {
                    _isProcessing = false;
                }
            });
        }
    }
}