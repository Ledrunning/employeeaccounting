﻿using Android.Gms.Vision.Faces;
using Android.Graphics;
using EA.Android.CustomControls;
using System;

namespace EA.Android
{
    /// <summary>
    /// Class for draww rectangle around the face
    /// and text printing on the screen
    /// </summary>
    public class FaceGraphic : Graphic
    {
        private static readonly float FacePositionRadius = 20.0f;

        //private static readonly float ID_TEXT_SIZE = 40.0f;
        private static readonly float IdTextSize = 40.0f;

        private static readonly float MidTextSize = 20.0f;
        private static readonly float IdYOffset = 50.0f;
        private static readonly float IdXOffset = -50.0f;
        private static readonly float BoxStrokeWidth = 5.0f;

        private static Color[] _colorChoices = {
                                                    Color.Blue,
                                                    Color.Cyan,
                                                    Color.Green,
                                                    Color.Magenta,
                                                    Color.Red,
                                                    Color.White,
                                                    Color.Yellow
                                               };

        private static int _currentColorIndex = 0;
        private Paint _facePositionPaint;
        private Paint _idPaint;
        private Paint _mIdPaint;
        private Paint _boxPaint;

        /// <summary>
        /// Using volatile because it's using in parallel task
        /// </summary>
        private volatile Face _face;

        private int _faceId;

        /// <summary>
        /// Constructor based from GraphicOverlay
        /// </summary>
        /// <param name="overlay"></param>
        public FaceGraphic(GraphicOverlay overlay) : base(overlay)
        {
            _currentColorIndex = (_currentColorIndex + 1) % _colorChoices.Length;
            var selectedColor = _colorChoices[_currentColorIndex];

            _facePositionPaint = new Paint()
            {
                Color = selectedColor
            };
            _idPaint = new Paint()
            {
                Color = selectedColor,
                TextSize = IdTextSize
            };
            _mIdPaint = new Paint()
            {
                Color = selectedColor,
                TextSize = MidTextSize
            };
            _boxPaint = new Paint()
            {
                Color = selectedColor
            };
            _boxPaint.SetStyle(Paint.Style.Stroke);
            _boxPaint.StrokeWidth = BoxStrokeWidth;
        }

        /// <summary>
        /// ID setter method
        /// </summary>
        /// <param name="id"></param>
        public void SetId(int id)
        {
            _faceId = id;
        }

        /// <summary>
        /// Updates the face instance from the detection of the most recent frame.  Invalidates the
        ///  relevant portions of the overlay to trigger a redraw.
        /// </summary>
        /// <param name="face"></param>
        public void UpdateFace(Face face)
        {
            _face = face;
            PostInvalidate();
        }

        /// <summary>
        /// Method for drawing rectangle around face
        /// </summary>
        /// <param name="canvas"></param>
        public override void Draw(Canvas canvas)
        {
            Face face = _face;

            if (face == null)
            {
                return;
            }

            // Draws a circle at the position of the detected face, with the face's track id below.
            float x = TranslateX(face.Position.X + face.Width / 2);
            float y = TranslateY(face.Position.Y + face.Height / 2);
            //canvas.DrawCircle(x, y, FACE_POSITION_RADIUS, mFacePositionPaint);

            //HACK: Demo only
            if (!string.IsNullOrEmpty(MainActivity.GreetingsText))

                // Drawwing text in video stream
                canvas.DrawText(MainActivity.GreetingsText, x + IdXOffset, y + IdYOffset, _idPaint);
            canvas.DrawText("Осман Мазинов", x + IdXOffset, y - 350, _idPaint);

            var mood = Math.Round(face.IsSmilingProbability, 2) * 100;
            var rightEye = Math.Round(face.IsLeftEyeOpenProbability, 2) * 100;
            var leftRight = Math.Round(face.IsRightEyeOpenProbability, 2) * 100;

            if (mood > 15)
            {
                canvas.DrawText("Настроение хорошее ", x - IdXOffset, y - IdYOffset, _mIdPaint);
            }
            else if (mood < 10)
            {
                canvas.DrawText("Настроение нейтральное ", x - IdXOffset, y - IdYOffset, _mIdPaint);
            }
            else
            {
                canvas.DrawText("Настроение плохое ", x - IdXOffset, y - IdYOffset, _mIdPaint);
            }

            if (rightEye > 50)
            {
                canvas.DrawText("Левый глаз открыт ", x + IdXOffset * 2, y - IdYOffset * 2, _mIdPaint);
            }
            else
            {
                canvas.DrawText("Левый глаз закрыт ", x + IdXOffset * 2, y - IdYOffset * 2, _mIdPaint);
            }

            if (leftRight > 50)
            {
                canvas.DrawText("Правый глаз открыт ", x + IdXOffset * 2, y + IdYOffset * 2, _mIdPaint);
            }
            else
            {
                canvas.DrawText("Правый глаз закрыт ", x + IdXOffset * 2, y + IdYOffset * 2, _mIdPaint);
            }

            //canvas.DrawText("Настроение: " + Math.Round(face.IsSmilingProbability, 2).ToString(), x - IdXOffset, y - IdYOffset, _mIdPaint);
            //canvas.DrawText("Правый глаз: " + Math.Round(face.IsRightEyeOpenProbability, 2).ToString(), x + IdXOffset * 2, y + IdYOffset * 2, _mIdPaint);
            //canvas.DrawText("Левый глаз: " + Math.Round(face.IsLeftEyeOpenProbability, 2).ToString(), x - IdXOffset * 2, y - IdYOffset * 2, _mIdPaint);

            // Draws a bounding box around the face.
            float xOffset = ScaleX(face.Width / 2.0f);
            float yOffset = ScaleY(face.Height / 2.0f);
            float left = x - xOffset;
            float top = y - yOffset;
            float right = x + xOffset;
            float bottom = y + yOffset;
            canvas.DrawRect(left, top, right, bottom, _boxPaint);
        }

        private void CalculateFaceFeatures(Face face, Canvas canvas)
        {
           
        }
    }
}